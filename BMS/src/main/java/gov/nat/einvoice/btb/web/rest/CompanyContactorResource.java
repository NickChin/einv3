package gov.nat.einvoice.btb.web.rest;

import gov.nat.einvoice.btb.domain.CompanyContactor;
import gov.nat.einvoice.btb.service.CompanyContactorService;
import gov.nat.einvoice.btb.web.rest.errors.BadRequestAlertException;
import gov.nat.einvoice.btb.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api/companycontactor")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CompanyContactorResource {
    private final Logger log = LoggerFactory.getLogger(CompanyContactorResource.class);

    private static final String ENTITY_NAME = "COMPANY_CONTACTOR";

    @Inject
    CompanyContactorService companyContactorService;

    /*
     * 取得營業人聯絡人資訊
     *
     *
     */
    @GET
    @Path("/{ban}")
    public Response readCompanyContactors(@PathParam("ban") String ban){
//        var result = companyContactorService.findOne(ban);
        List<CompanyContactor> result = companyContactorService.queryOne(ban);
        return Response.ok(result).build();
    }

    /*
     * 新增營業人聯絡人資訊
     *
     */
    @POST
    public Response createOrUpdateCompany(CompanyContactor companyContactor) {
        log.debug("REST request to create CompanyContactor : {}", companyContactor);
        if (companyContactor.getId().getBan() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "id null");
        }
        CompanyContactor result = companyContactorService.persistOrUpdate(companyContactor);
        Response.ResponseBuilder response = Response.ok().entity(result);
//        HeaderUtil.createEntityUpdateAlert(
//                "app",
//                true,
//                ENTITY_NAME,
//                companyContactor.getId().getBan()).forEach(response::header
//        );
        return response.build();
    }

    /*
     * 取得營業人聯絡人資訊
     *
     */

    /*
     * 更新營業人聯絡人資訊
     *
     */

    /*
     * 刪除營業人聯絡人資
     *
     */
}
