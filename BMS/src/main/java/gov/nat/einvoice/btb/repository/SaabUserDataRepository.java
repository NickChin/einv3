package gov.nat.einvoice.btb.repository;

import gov.nat.einvoice.btb.domain.SaabUserData;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SaabUserDataRepository implements PanacheRepositoryBase<SaabUserData, String> {

    public SaabUserData queryByUserId(String userId) {
        return find("userId", userId).firstResult();
    }

    public SaabUserData Registry(SaabUserData saabUserData) {
        persist(saabUserData);
        return saabUserData;
    }
}
