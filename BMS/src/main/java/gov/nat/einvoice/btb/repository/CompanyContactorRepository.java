package gov.nat.einvoice.btb.repository;

import gov.nat.einvoice.btb.domain.CompanyContactor;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Query;
import java.util.List;

@SuppressWarnings("unused")
@ApplicationScoped
public class CompanyContactorRepository implements PanacheRepositoryBase<CompanyContactor, String> {

    public CompanyContactor update(CompanyContactor companyContactor) {
        if (companyContactor == null) {
            throw new IllegalArgumentException("companyContactor can't be null");
        }
//        var entity = CompanyContactor.<CompanyContactor>findById(companyContactor.getId());
        CompanyContactor entity = CompanyContactor.<CompanyContactor>findById(companyContactor.getId());

        if (entity != null) {
            entity.setId(companyContactor.getId());
        }
        persist(entity);
        return entity;
    }

    public CompanyContactor persistOrUpdate(CompanyContactor companyContactor) {
        if (companyContactor == null) {
            throw new IllegalArgumentException("company can't be null");
        }
        if (find("ban",companyContactor.getId().getBan()).list().isEmpty()) {
            persist(companyContactor);
            return companyContactor;
        } else {
            return update(companyContactor);
        }
    }

    public List<CompanyContactor> findByBan(String ban) {
        return find("ban",ban).list();
    }

    public List<CompanyContactor> queryByBan(String ban) {
        Query query = getEntityManager().createNativeQuery(
                "SELECT * FROM COMPANY_CONTACTOR WHERE BAN = :ban", CompanyContactor.class);
        query.setParameter("ban",ban);
        return query.getResultList();
    }
}
