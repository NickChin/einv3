package gov.nat.einvoice.btb.web.rest;

import gov.nat.einvoice.btb.domain.Company;
import gov.nat.einvoice.btb.service.CompanyService;
import gov.nat.einvoice.btb.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import java.util.Optional;

@Path("/api/company")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class CompanyResource {
    private final Logger log = LoggerFactory.getLogger(CompanyResource.class);

    private static final String ENTITY_NAME = "company";

    @Inject
    CompanyService companyService;

    /*
     * 取得營業人資料
     *
     */
    @GET
    @Path("/{companyId}")
    public Response.ResponseBuilder readCompany(@PathParam("companyId") String companyId) {
        log.debug("REST request to get Company : {}", companyId);
        Optional<Company> company = companyService.findOne(companyId);
//        return ResponseUtil.wrapOrNotFound(company);
        return Response.ok().entity(company);
    }
    /*
     * 新增/更新 營業人資料
     *
     */
    @POST
    @Path("/company")
    public Response createOrUpdateCompany(Company company) {
        log.debug("REST request to create Company : {}", company);
        if (company.getCompId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "id null");
        }
        Company result = companyService.persistOrUpdate(company);
        Response.ResponseBuilder response = Response.ok().entity(result);
//        HeaderUtil.createEntityUpdateAlert("app", true, ENTITY_NAME, company.getCompId()).forEach(response::header);
        return response.build();
    }
}
