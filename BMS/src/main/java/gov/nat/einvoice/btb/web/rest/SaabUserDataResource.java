package gov.nat.einvoice.btb.web.rest;

import gov.nat.einvoice.btb.domain.SaabUserData;
import gov.nat.einvoice.btb.service.SaabUserDataService;
import gov.nat.einvoice.btb.web.rest.vm.UserInfo;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/bms")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SaabUserDataResource {
    private final Logger log = LoggerFactory.getLogger(SaabUserDataResource.class);

    @Inject
    SaabUserDataService saabUserDataService;

    @GET
    @Path("/loginPasswordPolicy")
    @Operation(summary = "登入檢核")
    public Response LoginPasswordPolicy(@BeanParam SaabUserData data) {
        log.debug("LoginPasswordPolicy : {}", data.userId + "," + data.userPassword);
        UserInfo userInfo = saabUserDataService.LoginPasswordPolicy(data.userId, data.userPassword);
        return Response.ok(userInfo).build();
    }

    @POST
    @Path("/registryPasswordPolicy")
    @Operation(summary = "註冊檢核")
    public Response RegistryPasswordPolicy(SaabUserData data) {
        log.debug("RegistryPasswordPolicy : {}", data.getUserId() + "," + data.getUserPassword() + "," + data.getUserName());
        UserInfo userInfo = saabUserDataService.RegistryPasswordPolicy(data.getUserId(), data.getUserPassword(), data.getUserName());
        return Response.ok(userInfo).build();
    }

}
