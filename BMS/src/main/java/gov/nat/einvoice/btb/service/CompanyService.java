package gov.nat.einvoice.btb.service;

import gov.nat.einvoice.btb.domain.Company;
import gov.nat.einvoice.btb.repository.CompanyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Optional;

@ApplicationScoped
@Transactional
public class CompanyService {
    private final Logger log = LoggerFactory.getLogger(CompanyService.class);

    @Inject
    CompanyRepository companyRepository;

    @Transactional
    public Company persistOrUpdate(Company company) {
        log.debug("Request to save Company : {}", company);
        return companyRepository.persistOrUpdate(company);
    }

    @Transactional
    public Optional<Company> findOne(String companyId) {
        log.debug("Request to get Company : {}",companyId);
        return companyRepository.findByIdOptional(companyId);
    }

}
