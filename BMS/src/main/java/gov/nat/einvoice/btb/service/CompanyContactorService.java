package gov.nat.einvoice.btb.service;

import gov.nat.einvoice.btb.domain.CompanyContactor;
import gov.nat.einvoice.btb.repository.CompanyContactorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
@Transactional
public class CompanyContactorService {
    private final Logger log = LoggerFactory.getLogger(CompanyContactorService.class);

    @Inject
    CompanyContactorRepository companyContactorRepository;

    @Transactional
    public CompanyContactor persistOrUpdate(CompanyContactor companyContactory) {
        log.debug("Request to save CompanyContactor : {}", companyContactory);
        return companyContactorRepository.persistOrUpdate(companyContactory);
    }

    @Transactional
    public List<CompanyContactor> findOne(String ban) {
        log.debug("Request to get CompanyContactor : {}", ban);
        return companyContactorRepository.findByBan(ban);
    }

    @Transactional
    public List<CompanyContactor> queryOne(String ban) {
        log.debug("Request to get CompanyContactor : {}", ban);
        return companyContactorRepository.queryByBan(ban);
    }
}
