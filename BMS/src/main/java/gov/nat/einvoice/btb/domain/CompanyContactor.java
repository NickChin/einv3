package gov.nat.einvoice.btb.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "COMPANY_CONTACTOR")
@RegisterForReflection
public class CompanyContactor extends PanacheEntityBase implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CompanyContactorPK id = new CompanyContactorPK();

	@Column(name = "COMP_NAME")
	private String compName;

    public CompanyContactorPK getId() {
        return id;
    }
    public void setId(CompanyContactorPK id) {
        this.id = id;
    }
    public String getCompName() {
        return compName;
    }
    public void setCompName(String compName) {
        this.compName = compName;
    }

    @Embeddable
    public static class CompanyContactorPK implements Serializable {

        private static final long serialVersionUID = 1L;

        @Column(name = "BAN")
        private String ban;

        @Column(name = "CONT_TYPE")
        private String contType;

        public String getBan() {
            return ban;
        }
        public void setBan(String ban) {
            this.ban = ban;
        }
        public String getContType() {
            return contType;
        }
        public void setContType(String contType) {
            this.contType = contType;
        }

        @Override
        public int hashCode() {
            return 31;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }

            if (!(o instanceof CompanyContactorPK)) {
                return false;
            }

            CompanyContactorPK other = (CompanyContactorPK) o;

            return Objects.equals(ban, other.ban);
        }
    }
}
