package gov.nat.einvoice.btb.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.persistence.*;
import javax.ws.rs.QueryParam;
import java.io.Serializable;

@Entity
@Table(name = "SAAB_USER_DATA")
@RegisterForReflection
public class SaabUserData extends PanacheEntityBase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "USER_ID")
    @QueryParam("userId")
    public String userId;

    @Column(name = "USER_PASSWORD")
    @QueryParam("userPassword")
    public String userPassword;

    @Column(name = "USER_NAME")
    private String UserName;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "PWD_CHG_INTERVAL")
    private String pwdChgInterval;

    @Column(name = "PWD_CHG_DATE")
    private String pwdChgDate;

    @Column(name = "EXPIRED_DATE")
    private String ExpiredDate;

    @Column(name = "LOGIN_LOCK_NUM")
    private String loginLockNum;

    @Column(name = "LOGIN_ERR_NUM")
    private String loginErrNum;

    @Column(name = "BUILD_DATE")
    private String buildDate;

    @Column(name = "COMPANY_BAN")
    private String CompanyBan;

    @Column(name = "BUILD_BY")
    private String buildBy;

    @Column(name = "TELEPHONE")
    private String telephone;

    @Column(name = "MOBILE")
    private String mobile;

    @Column(name = "USER_STATUS")
    private String userStatus;

    @Column(name = "NTAX_ID")
    private String ntaxId;

    @Column(name = "NTAX_NAME")
    private String ntaxName;

    @Column(name = "BANK_NO")
    private String bankNo;

    @Column(name = "BANK_ACCOUNT")
    private String bankAccount;

    @Column(name = "EXT_ID")
    private String extId;

    @Column(name = "EXT_NAME")
    private String extName;

    @Column(name = "USER_TYPE")
    private String userType;

    @Column(name = "CUSTOM_ID")
    private String customId;

    @Column(name = "ENABLE_REMIT")
    private String enableRemit;

    @Column(name = "EMAIL_INV")
    private String emailInv;

    @Column(name = "EMAIL_NOWIN")
    private String emailNowin;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPwdChgInterval() {
        return pwdChgInterval;
    }

    public void setPwdChgInterval(String pwdChgInterval) {
        this.pwdChgInterval = pwdChgInterval;
    }

    public String getPwdChgDate() {
        return pwdChgDate;
    }

    public void setPwdChgDate(String pwdChgDate) {
        this.pwdChgDate = pwdChgDate;
    }

    public String getExpiredDate() {
        return ExpiredDate;
    }

    public void setExpiredDate(String ExpiredDate) {
        this.ExpiredDate = ExpiredDate;
    }

    public String getLoginLockNum() {
        return loginLockNum;
    }

    public void setLoginLockNum(String loginLockNum) {
        this.loginLockNum = loginLockNum;
    }

    public String getLoginErrNum() {
        return loginErrNum;
    }

    public void setLoginErrNum(String loginErrNum) {
        this.loginErrNum = loginErrNum;
    }

    public String getBuildDate() {
        return buildDate;
    }

    public void setBuildDate(String buildDate) {
        this.buildDate = buildDate;
    }

    public String getCompanyBan() {
        return CompanyBan;
    }

    public void setCompanyBan(String companyBan) {
        CompanyBan = companyBan;
    }

    public String getBuildBy() {
        return buildBy;
    }

    public void setBuildBy(String buildBy) {
        this.buildBy = buildBy;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getNtaxId() {
        return ntaxId;
    }

    public void setNtaxId(String ntaxId) {
        this.ntaxId = ntaxId;
    }

    public String getNtaxName() {
        return ntaxName;
    }

    public void setNtaxName(String ntaxName) {
        this.ntaxName = ntaxName;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getExtId() {
        return extId;
    }

    public void setExtId(String extId) {
        this.extId = extId;
    }

    public String getExtName() {
        return extName;
    }

    public void setExtName(String extName) {
        this.extName = extName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public String getEnableRemit() {
        return enableRemit;
    }

    public void setEnableRemit(String enableRemit) {
        this.enableRemit = enableRemit;
    }

    public String getEmailInv() {
        return emailInv;
    }

    public void setEmailInv(String emailInv) {
        this.emailInv = emailInv;
    }

    public String getEmailNowin() {
        return emailNowin;
    }

    public void setEmailNowin(String emailNowin) {
        this.emailNowin = emailNowin;
    }

    @Override
    public String toString() {
        return "SaabUserData{" +
                "userId='" + userId + '\'' +
                ", UserPassword='" + userPassword + '\'' +
                ", UserName='" + UserName + '\'' +
                '}';
    }
}
