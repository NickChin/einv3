package gov.nat.einvoice.btb.web.rest.vm;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class UserInfo {

    private static final long serialVersionUID = 1L;

    private String status;

    private String message;

    private String message_code;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage_code() {
        return message_code;
    }

    public void setMessage_code(String message_code) {
        this.message_code = message_code;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", message_code='" + message_code + '\'' +
                '}';
    }
}
