package gov.nat.einvoice.btb.web.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class CheckUserInfoUtil {

    public static boolean hasUpperCase(String str) {
        boolean upperCaseFlag = false;
        if (!str.equals(null) || !str.equals("")) {
            for (int i = 0; i < str.length(); i++) {
                if (Character.isUpperCase(str.charAt(i))) {
                    upperCaseFlag = true;
                    break;
                }
            }
        }
        return upperCaseFlag;
    }

    public static boolean hasLowerCase(String str) {
        boolean lowerCaseFlag = false;
        if (!str.equals(null) || !str.equals("")) {
            for (int i = 0; i < str.length(); i++) {
                if (Character.isLowerCase(str.charAt(i))) {
                    lowerCaseFlag = true;
                    break;
                }
            }
        }
        return lowerCaseFlag;
    }

    public static boolean hasDigit(String str) {
        boolean digitFlag = false;
        if (!str.equals(null) || !str.equals("")) {
            for (int i = 0; i < str.length(); i++) {
                if (Character.isDigit(str.charAt(i))) {
                    digitFlag = true;
                    break;
                }
            }
        }
        return digitFlag;
    }

    public static boolean hasSpecialChar(String str) {
        boolean specialCharFlag = false;
        if (!str.equals(null) || !str.equals("")) {
            String reg = "[`~!@#$%^&*()-_=+,<.>/?;:\'\"[{]}\\|｀～！＠＃＄％＾＆（）－＿＝＋，＜．＞／？；：＇＂［｛］｝＼｜]+";
            for (int i = 0; i < reg.length(); i++) {
                if (str.contains(reg.substring(i, i + 1))) {
                    specialCharFlag = true;
                    break;
                }
            }
        }
        return specialCharFlag;
    }

    public static byte[] randSalt() {
        int saltLen = 8;
        byte[] b = new byte[saltLen];
        for (int i = 0; i < saltLen; i++) {
            byte bt = (byte) (int) (Math.random() * 256.0D - 128.0D);
            b[i] = bt;
        }
        return b;
    }

    public static byte[] encryptPwd(String password, byte[] salt) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-1");
            digest.update(password.getBytes());
            digest.update(salt);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return digest.digest();
    }

    public static String genPwd(String password, byte[] salt) {
        byte[] encryptPwd = encryptPwd(password, salt);
        byte[] merge = new byte[28];
        System.arraycopy(encryptPwd, 0, merge, 0, 20);
        System.arraycopy(salt, 0, merge, 20, 8);
        String encryptStr = Base64.getEncoder().encodeToString(merge);

        return encryptStr;
    }

    public static boolean checkPwd(String dbPwd, String loginPwd) {
        byte[] decordPwd = Base64.getDecoder().decode(dbPwd.getBytes());
        byte[] salt = new byte[8];
        byte[] sha1Password = new byte[20];
        System.arraycopy(decordPwd, 0, sha1Password, 0, 20);
        System.arraycopy(decordPwd, 20, salt, 0, 8);
        var encryptLoginPassword = encryptPwd(loginPwd, salt);
        return MessageDigest.isEqual(encryptLoginPassword, sha1Password);
    }

//    public static void main(String[] arg) {
//        System.out.println(checkPwd(genPwd("1qaz@WSX",randSalt()),"1qaz@WSX"));
//    }

}
