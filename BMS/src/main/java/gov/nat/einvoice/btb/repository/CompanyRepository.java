package gov.nat.einvoice.btb.repository;

import gov.nat.einvoice.btb.domain.Company;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import javax.enterprise.context.ApplicationScoped;

@SuppressWarnings("unused")
@ApplicationScoped
public class CompanyRepository implements PanacheRepositoryBase<Company, String> {

    public Company update(Company company) {
        if (company == null) {
            throw new IllegalArgumentException("company can't be null");
        }
        Company entity = Company.<Company>findById(company.getCompId());
        if (entity != null) {
//            待確認要更新哪些, 不然一次更新太多
            entity.setCompId(company.getCompId());
            entity.setAgtCd(company.getAgtCd());
            entity.setAssignType(company.getAssignType());
//            entity.
        }

        persist(entity);
        return entity;
    }

    public Company persistOrUpdate(Company company) {
        if (company == null) {
            throw new IllegalArgumentException("company can't be null");
        }
        if (findById(company.getCompId()) == null) {
            persist(company);
            return company;
        } else {
            return update(company);
        }
    }
}
