package gov.nat.einvoice.btb.domain;


import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
//import java.time.Instant;
import java.util.Date;

@Entity
@Table(name = "COMPANY")
@RegisterForReflection
public class Company extends PanacheEntityBase implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COMP_ID")
	private String compId;

	@Column(name="AGT_CD")
	private String agtCd;

	@Column(name="ASSIGN_TYPE")
	private String assignType;

	@Column(name="BAN_ADDR_HSN_CD")
	private String banAddrHsnCd;

	@Column(name="BAN_ADDR_HSN_NM")
	private String banAddrHsnNm;

	@Column(name="BAN_ADDR_LIN")
	private String banAddrLin;

	@Column(name="BAN_ADDR_ROAD_NO")
	private String banAddrRoadNo;

	@Column(name="BAN_ADDR_TOWN_CD")
	private String banAddrTownCd;

	@Column(name="BAN_ADDR_TOWN_NM")
	private String banAddrTownNm;

	@Column(name="BAN_ADDR_VILL_NM")
	private String banAddrVillNm;

	@Column(name="BEF_AFT_UPD_BAN")
	private String befAftUpdBan;

	@Column(name="BILL_ADDR")
	private String billAddr;

	@Column(name="BSCD")
	private String bscd;

	@Column(name="BSCD1")
	private String bscd1;

	@Column(name="BSCD2")
	private String bscd2;

	@Column(name="BSCD3")
	private String bscd3;

	@Column(name="CHAIRMAN")
	private String chairman;

	@Column(name="COMPANY_ADDR")
	private String companyAddr;

	@Column(name="COMPANY_BAN")
	private String companyBan;

	@Column(name="COMPANY_E_NAME")
	private String companyEName;

	@Column(name="COMPANY_E_S_NAME")
	private String companyESName;

	@Column(name="COMPANY_EMAIL")
	private String companyEmail;

	@Column(name="COMPANY_FAX")
	private String companyFax;

	@Column(name="COMPANY_GROUP")
	private String companyGroup;

	@Column(name="COMPANY_LEVEL")
	private BigDecimal companyLevel;

	@Column(name="COMPANY_NAME")
	private String companyName;

	@Column(name="COMPANY_S_NAME")
	private String companySName;

	@Column(name="COMPANY_TEL")
	private String companyTel;

	@Column(name="COMPANY_WEB")
	private String companyWeb;

	@Column(name="COMPANY_ZIP_CODE")
	private String companyZipCode;

	@Column(name="CONS_YEAR_MONTH")
	private String consYearMonth;

	@Column(name="CP_DATE")
	private Date cpDate;

	@Column(name="CTRL_DATE")
	private Date ctrlDate;

	@Column(name="CTRL_MK")
	private String ctrlMk;

	@Column(name="CTRL_REAS")
	private String ctrlReas;

	@Column(name="DEF_PASSWORD")
	private String defPassword;

	@Column(name="DELIVERY_ADDR")
	private String deliveryAddr;

	@Column(name="EINV_REG_DATE")
	private Date einvRegDate;

	@Column(name="ESTAB_DATE")
	private String estabDate;

	@Column(name="ESTAB_LOSN_MK")
	private String estabLosnMk;

	@Column(name="FALSE_BUSI_MK")
	private String falseBusiMk;

	@Column(name="GEN_BK_CNT")
	private BigDecimal genBkCnt;

	@Column(name="GEN_SP_BK_CNT")
	private BigDecimal genSpBkCnt;

	@Column(name="GLN_NAME")
	private String glnName;

	@Column(name="GLN_NO")
	private String glnNo;

	@Column(name="GOVE_PRIV_TP")
	private String govePrivTp;

	@Column(name="HEAD_BAN")
	private String headBan;

	@Column(name="HEADQ_BR_CD")
	private String headqBrCd;

	@Column(name="IMPT_UPDATE_REAS")
	private String imptUpdateReas;

	@Column(name="IS_BLACK_LIST")
	private String isBlackList;

	@Column(name="IS_EINVOICE_ACTIVED")
	private String isEinvoiceActived;

	@Column(name="KEYIN_DATE")
	private String keyinDate;

	@Column(name="LEVY_MODE1")
	private String levyMode1;

	@Column(name="NET_C2CV_BK_CNT")
	private BigDecimal netC2cvBkCnt;

	@Column(name="NET_C2V_BK_CNT")
	private BigDecimal netC2vBkCnt;

	@Column(name="NET_C3CV_BK_CNT")
	private BigDecimal netC3cvBkCnt;

	@Column(name="NET_C3V_BK_CNT")
	private BigDecimal netC3vBkCnt;

	@Column(name="NET_PC_BK_CNT")
	private BigDecimal netPcBkCnt;

	@Column(name="NET_SP_TAX_BK_CNT")
	private BigDecimal netSpTaxBkCnt;

	@Column(name="ORGN_TP")
	private String orgnTp;

	@Column(name="PERSONNEL_NUM")
	private BigDecimal personnelNum;

	private String principal;

	@Column(name="PUBLIC_SERVICE")
	private String publicService;

	@Column(name="QUEUE_NAME")
	private String queueName;

	@Column(name="RECEIPT_ADDR")
	private String receiptAddr;

	@Column(name="RECEIPT_TITLE")
	private String receiptTitle;

	@Column(name="RECV_DATE")
	private String recvDate;

	@Column(name="REMARK")
	private String remark;

	@Column(name="REOPEN_DATE")
	private String reopenDate;

	@Column(name="RESP_IDN")
	private String respIdn;

	@Column(name="STATUS")
	private String status;

	@Column(name="STATUS_DESC")
	private String statusDesc;

	@Column(name="STOP_BUY_MK")
	private String stopBuyMk;

	@Column(name="STOP_BUY_REAS")
	private String stopBuyReas;

	@Column(name="TAX_CENTER_ID")
	private String taxCenterId;

	@Column(name="TAX_CENTER_NAME")
	private String taxCenterName;

	@Column(name="TAX_RATE")
	private BigDecimal taxRate;

	@Column(name="TRANSFER_AGENT_BAN")
	private String transferAgentBan;

	@Column(name="UPD_CID")
	private String updCid;

	@Column(name="UPD_DT")
	private Date updDt;

	@Column(name="UPD_UID")
	private String updUid;

	@Column(name="UPDATE_DATE_TIME")
	private String updateDateTime;

	@Column(name="UPDATE_REAS_CD1")
	private String updateReasCd1;

	@Column(name="UPDATE_REAS_CD2")
	private String updateReasCd2;

	@Column(name="UPDATE_REAS_CD3")
	private String updateReasCd3;

	@Column(name="UPDATE_REAS_CD4")
	private String updateReasCd4;

	@Column(name="UPDATE_USER_CD")
	private String updateUserCd;

	@Column(name="VAT_LOSN")
	private String vatLosn;

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getAgtCd() {
        return agtCd;
    }

    public void setAgtCd(String agtCd) {
        this.agtCd = agtCd;
    }

    public String getAssignType() {
        return assignType;
    }

    public void setAssignType(String assignType) {
        this.assignType = assignType;
    }

    public String getBanAddrHsnCd() {
        return banAddrHsnCd;
    }

    public void setBanAddrHsnCd(String banAddrHsnCd) {
        this.banAddrHsnCd = banAddrHsnCd;
    }

    public String getBanAddrHsnNm() {
        return banAddrHsnNm;
    }

    public void setBanAddrHsnNm(String banAddrHsnNm) {
        this.banAddrHsnNm = banAddrHsnNm;
    }

    public String getBanAddrLin() {
        return banAddrLin;
    }

    public void setBanAddrLin(String banAddrLin) {
        this.banAddrLin = banAddrLin;
    }

    public String getBanAddrRoadNo() {
        return banAddrRoadNo;
    }

    public void setBanAddrRoadNo(String banAddrRoadNo) {
        this.banAddrRoadNo = banAddrRoadNo;
    }

    public String getBanAddrTownCd() {
        return banAddrTownCd;
    }

    public void setBanAddrTownCd(String banAddrTownCd) {
        this.banAddrTownCd = banAddrTownCd;
    }

    public String getBanAddrTownNm() {
        return banAddrTownNm;
    }

    public void setBanAddrTownNm(String banAddrTownNm) {
        this.banAddrTownNm = banAddrTownNm;
    }

    public String getBanAddrVillNm() {
        return banAddrVillNm;
    }

    public void setBanAddrVillNm(String banAddrVillNm) {
        this.banAddrVillNm = banAddrVillNm;
    }

    public String getBefAftUpdBan() {
        return befAftUpdBan;
    }

    public void setBefAftUpdBan(String befAftUpdBan) {
        this.befAftUpdBan = befAftUpdBan;
    }

    public String getBillAddr() {
        return billAddr;
    }

    public void setBillAddr(String billAddr) {
        this.billAddr = billAddr;
    }

    public String getBscd() {
        return bscd;
    }

    public void setBscd(String bscd) {
        this.bscd = bscd;
    }

    public String getBscd1() {
        return bscd1;
    }

    public void setBscd1(String bscd1) {
        this.bscd1 = bscd1;
    }

    public String getBscd2() {
        return bscd2;
    }

    public void setBscd2(String bscd2) {
        this.bscd2 = bscd2;
    }

    public String getBscd3() {
        return bscd3;
    }

    public void setBscd3(String bscd3) {
        this.bscd3 = bscd3;
    }

    public String getChairman() {
        return chairman;
    }

    public void setChairman(String chairman) {
        this.chairman = chairman;
    }

    public String getCompanyAddr() {
        return companyAddr;
    }

    public void setCompanyAddr(String companyAddr) {
        this.companyAddr = companyAddr;
    }

    public String getCompanyBan() {
        return companyBan;
    }

    public void setCompanyBan(String companyBan) {
        this.companyBan = companyBan;
    }

    public String getCompanyEName() {
        return companyEName;
    }

    public void setCompanyEName(String companyEName) {
        this.companyEName = companyEName;
    }

    public String getCompanyESName() {
        return companyESName;
    }

    public void setCompanyESName(String companyESName) {
        this.companyESName = companyESName;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyFax() {
        return companyFax;
    }

    public void setCompanyFax(String companyFax) {
        this.companyFax = companyFax;
    }

    public String getCompanyGroup() {
        return companyGroup;
    }

    public void setCompanyGroup(String companyGroup) {
        this.companyGroup = companyGroup;
    }

    public BigDecimal getCompanyLevel() {
        return companyLevel;
    }

    public void setCompanyLevel(BigDecimal companyLevel) {
        this.companyLevel = companyLevel;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanySName() {
        return companySName;
    }

    public void setCompanySName(String companySName) {
        this.companySName = companySName;
    }

    public String getCompanyTel() {
        return companyTel;
    }

    public void setCompanyTel(String companyTel) {
        this.companyTel = companyTel;
    }

    public String getCompanyWeb() {
        return companyWeb;
    }

    public void setCompanyWeb(String companyWeb) {
        this.companyWeb = companyWeb;
    }

    public String getCompanyZipCode() {
        return companyZipCode;
    }

    public void setCompanyZipCode(String companyZipCode) {
        this.companyZipCode = companyZipCode;
    }

    public String getConsYearMonth() {
        return consYearMonth;
    }

    public void setConsYearMonth(String consYearMonth) {
        this.consYearMonth = consYearMonth;
    }

    public Date getCpDate() {
        return cpDate;
    }

    public void setCpDate(Date cpDate) {
        this.cpDate = cpDate;
    }

    public Date getCtrlDate() {
        return ctrlDate;
    }

    public void setCtrlDate(Date ctrlDate) {
        this.ctrlDate = ctrlDate;
    }

    public String getCtrlMk() {
        return ctrlMk;
    }

    public void setCtrlMk(String ctrlMk) {
        this.ctrlMk = ctrlMk;
    }

    public String getCtrlReas() {
        return ctrlReas;
    }

    public void setCtrlReas(String ctrlReas) {
        this.ctrlReas = ctrlReas;
    }

    public String getDefPassword() {
        return defPassword;
    }

    public void setDefPassword(String defPassword) {
        this.defPassword = defPassword;
    }

    public String getDeliveryAddr() {
        return deliveryAddr;
    }

    public void setDeliveryAddr(String deliveryAddr) {
        this.deliveryAddr = deliveryAddr;
    }

    public Date getEinvRegDate() {
        return einvRegDate;
    }

    public void setEinvRegDate(Date einvRegDate) {
        this.einvRegDate = einvRegDate;
    }

    public String getEstabDate() {
        return estabDate;
    }

    public void setEstabDate(String estabDate) {
        this.estabDate = estabDate;
    }

    public String getEstabLosnMk() {
        return estabLosnMk;
    }

    public void setEstabLosnMk(String estabLosnMk) {
        this.estabLosnMk = estabLosnMk;
    }

    public String getFalseBusiMk() {
        return falseBusiMk;
    }

    public void setFalseBusiMk(String falseBusiMk) {
        this.falseBusiMk = falseBusiMk;
    }

    public BigDecimal getGenBkCnt() {
        return genBkCnt;
    }

    public void setGenBkCnt(BigDecimal genBkCnt) {
        this.genBkCnt = genBkCnt;
    }

    public BigDecimal getGenSpBkCnt() {
        return genSpBkCnt;
    }

    public void setGenSpBkCnt(BigDecimal genSpBkCnt) {
        this.genSpBkCnt = genSpBkCnt;
    }

    public String getGlnName() {
        return glnName;
    }

    public void setGlnName(String glnName) {
        this.glnName = glnName;
    }

    public String getGlnNo() {
        return glnNo;
    }

    public void setGlnNo(String glnNo) {
        this.glnNo = glnNo;
    }

    public String getGovePrivTp() {
        return govePrivTp;
    }

    public void setGovePrivTp(String govePrivTp) {
        this.govePrivTp = govePrivTp;
    }

    public String getHeadBan() {
        return headBan;
    }

    public void setHeadBan(String headBan) {
        this.headBan = headBan;
    }

    public String getHeadqBrCd() {
        return headqBrCd;
    }

    public void setHeadqBrCd(String headqBrCd) {
        this.headqBrCd = headqBrCd;
    }

    public String getImptUpdateReas() {
        return imptUpdateReas;
    }

    public void setImptUpdateReas(String imptUpdateReas) {
        this.imptUpdateReas = imptUpdateReas;
    }

    public String getIsBlackList() {
        return isBlackList;
    }

    public void setIsBlackList(String isBlackList) {
        this.isBlackList = isBlackList;
    }

    public String getIsEinvoiceActived() {
        return isEinvoiceActived;
    }

    public void setIsEinvoiceActived(String isEinvoiceActived) {
        this.isEinvoiceActived = isEinvoiceActived;
    }

    public String getKeyinDate() {
        return keyinDate;
    }

    public void setKeyinDate(String keyinDate) {
        this.keyinDate = keyinDate;
    }

    public String getLevyMode1() {
        return levyMode1;
    }

    public void setLevyMode1(String levyMode1) {
        this.levyMode1 = levyMode1;
    }

    public BigDecimal getNetC2cvBkCnt() {
        return netC2cvBkCnt;
    }

    public void setNetC2cvBkCnt(BigDecimal netC2cvBkCnt) {
        this.netC2cvBkCnt = netC2cvBkCnt;
    }

    public BigDecimal getNetC2vBkCnt() {
        return netC2vBkCnt;
    }

    public void setNetC2vBkCnt(BigDecimal netC2vBkCnt) {
        this.netC2vBkCnt = netC2vBkCnt;
    }

    public BigDecimal getNetC3cvBkCnt() {
        return netC3cvBkCnt;
    }

    public void setNetC3cvBkCnt(BigDecimal netC3cvBkCnt) {
        this.netC3cvBkCnt = netC3cvBkCnt;
    }

    public BigDecimal getNetC3vBkCnt() {
        return netC3vBkCnt;
    }

    public void setNetC3vBkCnt(BigDecimal netC3vBkCnt) {
        this.netC3vBkCnt = netC3vBkCnt;
    }

    public BigDecimal getNetPcBkCnt() {
        return netPcBkCnt;
    }

    public void setNetPcBkCnt(BigDecimal netPcBkCnt) {
        this.netPcBkCnt = netPcBkCnt;
    }

    public BigDecimal getNetSpTaxBkCnt() {
        return netSpTaxBkCnt;
    }

    public void setNetSpTaxBkCnt(BigDecimal netSpTaxBkCnt) {
        this.netSpTaxBkCnt = netSpTaxBkCnt;
    }

    public String getOrgnTp() {
        return orgnTp;
    }

    public void setOrgnTp(String orgnTp) {
        this.orgnTp = orgnTp;
    }

    public BigDecimal getPersonnelNum() {
        return personnelNum;
    }

    public void setPersonnelNum(BigDecimal personnelNum) {
        this.personnelNum = personnelNum;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getPublicService() {
        return publicService;
    }

    public void setPublicService(String publicService) {
        this.publicService = publicService;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getReceiptAddr() {
        return receiptAddr;
    }

    public void setReceiptAddr(String receiptAddr) {
        this.receiptAddr = receiptAddr;
    }

    public String getReceiptTitle() {
        return receiptTitle;
    }

    public void setReceiptTitle(String receiptTitle) {
        this.receiptTitle = receiptTitle;
    }

    public String getRecvDate() {
        return recvDate;
    }

    public void setRecvDate(String recvDate) {
        this.recvDate = recvDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReopenDate() {
        return reopenDate;
    }

    public void setReopenDate(String reopenDate) {
        this.reopenDate = reopenDate;
    }

    public String getRespIdn() {
        return respIdn;
    }

    public void setRespIdn(String respIdn) {
        this.respIdn = respIdn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getStopBuyMk() {
        return stopBuyMk;
    }

    public void setStopBuyMk(String stopBuyMk) {
        this.stopBuyMk = stopBuyMk;
    }

    public String getStopBuyReas() {
        return stopBuyReas;
    }

    public void setStopBuyReas(String stopBuyReas) {
        this.stopBuyReas = stopBuyReas;
    }

    public String getTaxCenterId() {
        return taxCenterId;
    }

    public void setTaxCenterId(String taxCenterId) {
        this.taxCenterId = taxCenterId;
    }

    public String getTaxCenterName() {
        return taxCenterName;
    }

    public void setTaxCenterName(String taxCenterName) {
        this.taxCenterName = taxCenterName;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public String getTransferAgentBan() {
        return transferAgentBan;
    }

    public void setTransferAgentBan(String transferAgentBan) {
        this.transferAgentBan = transferAgentBan;
    }

    public String getUpdCid() {
        return updCid;
    }

    public void setUpdCid(String updCid) {
        this.updCid = updCid;
    }

    public Date getUpdDt() {
        return updDt;
    }

    public void setUpdDt(Date updDt) {
        this.updDt = updDt;
    }

    public String getUpdUid() {
        return updUid;
    }

    public void setUpdUid(String updUid) {
        this.updUid = updUid;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public String getUpdateReasCd1() {
        return updateReasCd1;
    }

    public void setUpdateReasCd1(String updateReasCd1) {
        this.updateReasCd1 = updateReasCd1;
    }

    public String getUpdateReasCd2() {
        return updateReasCd2;
    }

    public void setUpdateReasCd2(String updateReasCd2) {
        this.updateReasCd2 = updateReasCd2;
    }

    public String getUpdateReasCd3() {
        return updateReasCd3;
    }

    public void setUpdateReasCd3(String updateReasCd3) {
        this.updateReasCd3 = updateReasCd3;
    }

    public String getUpdateReasCd4() {
        return updateReasCd4;
    }

    public void setUpdateReasCd4(String updateReasCd4) {
        this.updateReasCd4 = updateReasCd4;
    }

    public String getUpdateUserCd() {
        return updateUserCd;
    }

    public void setUpdateUserCd(String updateUserCd) {
        this.updateUserCd = updateUserCd;
    }

    public String getVatLosn() {
        return vatLosn;
    }

    public void setVatLosn(String vatLosn) {
        this.vatLosn = vatLosn;
    }
}
