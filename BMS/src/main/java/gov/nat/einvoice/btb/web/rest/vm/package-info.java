/**
 * View Models used by Quarkus MVC REST controllers.
 */
package gov.nat.einvoice.btb.web.rest.vm;
