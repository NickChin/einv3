package gov.nat.einvoice.btb.service;

import gov.nat.einvoice.btb.domain.SaabUserData;
import gov.nat.einvoice.btb.web.rest.vm.UserInfo;
import gov.nat.einvoice.btb.repository.SaabUserDataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import gov.nat.einvoice.btb.web.util.CheckUserInfoUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

@ApplicationScoped
@Transactional
public class SaabUserDataService {
    private final Logger log = LoggerFactory.getLogger(SaabUserDataService.class);

    @Inject
    SaabUserDataRepository saabUserDataRepository;

    @Transactional
    public UserInfo LoginPasswordPolicy(String userId, String password) {
        log.debug("LoginPasswordPolicy : {}", userId + "," + password);
        UserInfo userInfo = new UserInfo();

        if (userId.equals("") || password.equals("")) {
            userInfo.setStatus("error");
            userInfo.setMessage("帳密錯誤");
            userInfo.setMessage_code("AP001");
            return userInfo;
        } else {
            SaabUserData saabUserData = saabUserDataRepository.queryByUserId(userId);

            if (Optional.ofNullable(saabUserData).isEmpty()) {
                userInfo.setStatus("error");
                userInfo.setMessage("帳號不存在");
                userInfo.setMessage_code("AN001");
                return userInfo;
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date expiredDate = null;
                Date now = null;

                if (!Optional.ofNullable(saabUserData.getExpiredDate()).isEmpty()) {
                    try {
                        expiredDate = sdf.parse(saabUserData.getExpiredDate());
                        now = sdf.parse(LocalDate.now().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                String userStatus = "";
                if (!Optional.ofNullable(saabUserData.getUserStatus()).isEmpty()) {
                    userStatus = saabUserData.getUserStatus();
                }

                if (!CheckUserInfoUtil.checkPwd(saabUserData.getUserPassword(),password)) {
                    userInfo.setStatus("error");
                    userInfo.setMessage("帳密錯誤");
                    userInfo.setMessage_code("AP001");
                    return userInfo;
                } else if (userStatus.equals("L")) {
                    userInfo.setStatus("error");
                    userInfo.setMessage("帳號被鎖");
                    userInfo.setMessage_code("AL001");
                    return userInfo;
                } else if (expiredDate != null && now != null && expiredDate.before(now)) {
                    userInfo.setStatus("error");
                    userInfo.setMessage("密碼過期，請變更密碼");
                    userInfo.setMessage_code("AE001");
                    return userInfo;
                } else {
                    userInfo.setStatus("success");
                    userInfo.setMessage("核可");
                    userInfo.setMessage_code("AS001");
                    return userInfo;
                }
            }
        }
    }

    @Transactional
    public UserInfo RegistryPasswordPolicy(String userId, String userPassword, String userName) {
        log.debug("RegistryPasswordPolicy : {}", userId + "," + userPassword + "," + userName);
        UserInfo userInfo = new UserInfo();
        if (userId.equals("") || userPassword.equals("")) {
            userInfo.setStatus("error");
            userInfo.setMessage("帳密錯誤");
            userInfo.setMessage_code("AP001");
            return userInfo;
        } else {
            if (!CheckUserInfoUtil.hasUpperCase(userPassword) || !CheckUserInfoUtil.hasLowerCase(userPassword) || !CheckUserInfoUtil.hasDigit(userPassword) || !CheckUserInfoUtil.hasSpecialChar(userPassword)) {
                userInfo.setStatus("error");
                userInfo.setMessage("請檢查密碼複雜度");
                userInfo.setMessage_code("AP002");
                return userInfo;
            } else {
                Optional<SaabUserData> saabUserDatas = Optional.ofNullable(saabUserDataRepository.queryByUserId(userId));
                if (saabUserDatas.isPresent()) {
                    userInfo.setStatus("error");
                    userInfo.setMessage("帳號已註冊過");
                    userInfo.setMessage_code("AN002");
                    return userInfo;
                } else {
                    String shaPassword = CheckUserInfoUtil.genPwd(userPassword,CheckUserInfoUtil.randSalt());
                    SaabUserData saabUserData = new SaabUserData();
                    saabUserData.setUserId(userId);
                    saabUserData.setUserPassword(shaPassword);
                    saabUserData.setUserName(userName);
                    saabUserData.setStatus("U");
                    saabUserData.setBuildDate(LocalDate.now().toString());
                    LocalDate expiredDate = LocalDate.now().plusDays(90);
                    saabUserData.setExpiredDate(expiredDate.toString());
                    SaabUserData rstData = saabUserDataRepository.Registry(saabUserData);
                    if (!rstData.getUserId().equals(null)) {
                        userInfo.setStatus("success");
                        userInfo.setMessage("註冊成功");
                        userInfo.setMessage_code("AS002");
                        return userInfo;
                    }
                }
            }
            return userInfo;
        }
    }

}
